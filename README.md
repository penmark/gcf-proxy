# GCF Proxy

## First time deploy
https://cloud.google.com/sdk/gcloud/reference/functions/deploy
```
gcloud beta functions deploy <func_name> --region us-east1 --entry-point handler --memory 128 --trigger-http --runtime nodejs10 --set-env-vars URL=<url>
```
